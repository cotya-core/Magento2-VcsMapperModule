<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var SchemaSetupInterface
     */
    private $installer;
    
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->installer = $installer = $setup;
        
        $installer->startSetup();
        
        $this->createTables();

        $installer->endSetup();
    }
    
    private function getTable($name)
    {
        return $this->installer->getTable('cotya_vcsmapper_'.$name);
    }
    
    private function createTables()
    {
        $installer = $this->installer;

        $table = $installer->getConnection()->newTable(
            $this->getTable('repository')
        )->addColumn(
            'repository_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT,
            null,
            ['nullable' => false, 'primary' => true, 'identity' => true]
        )->addColumn(
            'uri',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, ]
        )->addIndex(
            $installer->getIdxName($this->getTable('repository'), ['uri']),
            ['uri']
        );
        $installer->getConnection()->createTable($table);
        
        $table = $installer->getConnection()->newTable(
            $this->getTable('version')
        )->addColumn(
            'version_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT,
            null,
            ['nullable' => false, 'primary' => true, 'identity' => true]
        )->addColumn(
            'repository_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT,
            null,
            ['nullable' => false, ]
        )->addColumn(
            'semver',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, ]
        )->addColumn(
            'tag',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true, ]
        )->addColumn(
            'meta',
            \Magento\Framework\DB\Ddl\Table::TYPE_BLOB,
            null,
            ['nullable' => true, ]
        )->addIndex(
            $installer->getIdxName($this->getTable('version'), ['repository_id']),
            ['repository_id']
        );
        $installer->getConnection()->createTable($table);
    }
}
