<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Console;

use Cotya\VcsMapper\Model\Cron;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronCommand extends Command
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('cotya:vcsmapper:cron');
        $this->setDescription('execute cron as cli command');
        $this->addArgument(
            'cron',
            InputArgument::REQUIRED,
            ''
        );
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('start');
        $cron = $input->getArgument('cron');

        switch ($cron) {
            case 'repositoryRefresh':
                $this->objectManager->create(Cron::class)->repositoryRefresh();
                break;
            default:
                $output->writeln('cron not found');
        };



        $output->writeln('finished execution of this command');
    }
}
