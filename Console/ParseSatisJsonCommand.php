<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Console;

use Composer\Semver\Comparator;
use Composer\Semver\VersionParser;
use Cotya\Queue\Model\QueueService;
use Cotya\VcsMapper\Model\RepositoryManager;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseSatisJsonCommand extends Command
{
    
    /** @var ScopeConfigInterface  */
    protected $scopeConfig;

    /**
     * @var RepositoryManager
     */
    protected $repositoryManager;

    /**
     * @var QueueService
     */
    protected $queueService;
    
    /**
     * Constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        QueueService $queueService,
        RepositoryManager $repositoryManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->repositoryManager = $repositoryManager;
        $this->queueService = $queueService;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('cotya:vcsmapper:parse-satis');
        $this->setDescription('fetch vcs repositories');
        $this->addArgument(
            'file',
            InputArgument::REQUIRED,
            'Path to a satis.json file'
        );
        parent::configure();
    }
    
    private function getStoreConfigValue($key)
    {
        return $this->scopeConfig->getValue($key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getArgument('file');
        $satisJson = json_decode(file_get_contents($filePath), true);
        
        foreach ($satisJson['repositories'] as $entry) {
            if ($entry['type'] === 'vcs') {
                $uri = $entry['url'];
                if ($this->repositoryManager->getRepositoryByUri($uri) === null) {
                    if ($output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                        $output->writeln('add '.$uri);
                    }
                    $this->repositoryManager->addRepository($uri);
                } else {
                    if ($output->getVerbosity() >= OutputInterface::VERBOSITY_VERY_VERBOSE) {
                        $output->writeln('was already there: '.$uri);
                    }
                }
            }
        }
        
        $output->writeln('finished execution of this command');
    }
}
