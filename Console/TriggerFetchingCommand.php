<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Console;

use Composer\Semver\Comparator;
use Composer\Semver\VersionParser;
use Cotya\Queue\Model\QueueService;
use Cotya\VcsMapper\Model\RepositoryManager;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TriggerFetchingCommand extends Command
{
    
    /** @var ScopeConfigInterface  */
    protected $scopeConfig;

    /**
     * @var RepositoryManager
     */
    protected $repositoryManager;

    /**
     * @var QueueService
     */
    protected $queueService;
    
    /**
     * Constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        QueueService $queueService,
        RepositoryManager $repositoryManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->repositoryManager = $repositoryManager;
        $this->queueService = $queueService;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('cotya:vcsmapper:fetch');
        $this->setDescription('fetch vcs repositories');
        parent::configure();
    }
    
    private function getStoreConfigValue($key)
    {
        return $this->scopeConfig->getValue($key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->repositoryManager->addRepository('http://test2.example.com');
        $this->queueService->add('element 1', 'test-queue');
        $this->queueService->add('element 2', 'test-queue');
        ld($this->queueService->getSingleElement('test-queue'));
        $repository = $this->repositoryManager->getRepositoryByUri('http://example.com');
        ld($this->repositoryManager->getVersionsOfRepository($repository));
        
        $output->writeln('Example:');
        $output->writeln($this->getStoreConfigValue('cotya_vcs_mapper/main/execution_limit'));
        $output->writeln('finished execution of this command');
    }
}
