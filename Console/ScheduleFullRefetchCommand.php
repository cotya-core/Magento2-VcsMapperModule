<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Console;

use Composer\Semver\Comparator;
use Composer\Semver\VersionParser;
use Cotya\Queue\Model\QueueService;
use Cotya\VcsMapper\Model\RepositoryManager;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScheduleFullRefetchCommand extends Command
{
    
    /** @var ScopeConfigInterface  */
    protected $scopeConfig;

    /**
     * @var RepositoryManager
     */
    protected $repositoryManager;

    /**
     * @var QueueService
     */
    protected $queueService;
    
    /**
     * Constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        QueueService $queueService,
        RepositoryManager $repositoryManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->repositoryManager = $repositoryManager;
        $this->queueService = $queueService;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('cotya:vcsmapper:schedule-full-refetch');
        $this->setDescription('schedule all repositories for a refetch');
        parent::configure();
    }
    
    private function getStoreConfigValue($key)
    {
        return $this->scopeConfig->getValue($key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $repositories = $this->repositoryManager->getAllRepositories();
        $number = 0;
        foreach ($repositories as $repository) {
            $this->repositoryManager->addRepositoryToQueue($repository);
            $number++;
        }
        
        $output->writeln("added $number of repositories to queue");
        $output->writeln('finished execution of this command');
    }
}
