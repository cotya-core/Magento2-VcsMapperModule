<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Model;


use Composer\Semver\VersionParser;
use Cotya\Queue\Model\QueueService;
use Magento\Framework\ObjectManagerInterface;

class RepositoryManager
{
    const QUEUE_NAME = 'cotya_vcsmapper/repository_refresh';
    
    /**
     * @var Resource\Repository
     */
    protected $repositoryResource;

    /**
     * @var Resource\Version
     */
    protected $versionResource;
    
    /**
     * @var VersionParser
     */
    protected $semverParser;

    /**
     * @var QueueService
     */
    protected $queueService;
    
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    
    public function __construct(
        Resource\Repository $repositoryResource,
        Resource\Version $versionResource,
        VersionParser $semverParser,
        QueueService $queueService,
        ObjectManagerInterface $objectManager
    ) {
        $this->repositoryResource = $repositoryResource;
        $this->versionResource = $versionResource;
        $this->semverParser = $semverParser;
        $this->queueService = $queueService;
        $this->objectManager = $objectManager;
    }

    public function addRepository($uri)
    {
        $repository = $this->objectManager->create(Repository::class);
        $repository->setUri($uri);
        $this->repositoryResource->save($repository);
    }
    
    public function getRepositoryByUri($uri)
    {
        $repository = $this->objectManager->create(Repository::class);
        $repository->load($uri, 'uri');
        if ($repository->getId() === null) {
            $repository = null;
        }
        return $repository;
    }
    
    public function addVersionToRepository(Repository $repository, Version $version)
    {
        $version->setRepositoryId($repository->getId());
        $this->versionResource->save($version);
    }

    /**
     * @param $semver
     *
     * @throws \UnexpectedValueException
     * @return Version
     */
    public function buildVersionObject($semver)
    {
        $this->semverParser->normalize($semver); // try if it is valid, throws exception if not
        $version = $this->objectManager->create(Version::class);
        $version->setSemver($semver);
        return $version;
    }

    /**
     * @return array
     */
    public function getAllRepositories()
    {
        /** @var Resource\Repository\Collection $collection */
        $collection = $this->objectManager->create(Resource\Repository\Collection::class);
        $collection->addFieldToSelect('*');
        return $collection;
    }
    
    public function getVersionsOfRepository(Repository $repository)
    {
        $result = null;
        /** @var Resource\Version\Collection $collection */
        //$collection = $this->versionResource->getCollection();
        $collection = $this->objectManager->create(Resource\Version\Collection::class);
        $result = $collection->addFieldToFilter('repository_id', $repository->getId());
        return $result;
    }
    
    public function addRepositoryToQueue(Repository $repository)
    {
        if ($repository->getId()) {
            $this->queueService->add($repository->getId(), self::QUEUE_NAME);
        }
    }

    /**
     * @return Repository|null
     */
    public function getRepositoryForRefreshFromQueue()
    {
        $entry = $this->queueService->getSingleElement(self::QUEUE_NAME);
        if ($entry===null) {
            return null;
        }
        $repository = $this->objectManager->create(Repository::class);
        //ld($entry);
        $repository->load($entry);
        if ($repository->getId() === null) {
            return null;
        }
        return $repository;
    }
    
}
