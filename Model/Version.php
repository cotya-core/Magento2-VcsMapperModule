<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Model;

/**
 * Class Version
 *
 * @method setRepositoryId(integer $id)
 * @method integer getRepositoryId()
 * @method setSemver(string $semver)
 * @method setTag(string $tag)
 * @method setMeta(string $json)
 * @method integer getSemver()
 * 
 * @package Cotya\VcsMapper
 */
class Version extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init('Cotya\VcsMapper\Model\Resource\Version');
    }
}
