<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Cron
{

    /**
     * @var RepositoryManager
     */
    protected $repositoryManager;

    /**
     * @var \Github\Client
     */
    protected $githubClient;

    /** @var ScopeConfigInterface  */
    protected $scopeConfig;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param RepositoryManager $repositoryManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Github\Client    $githubClient
     */
    public function __construct(
        RepositoryManager $repositoryManager,
        ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        \Github\Client $githubClient
    ) {
        $this->repositoryManager = $repositoryManager;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->githubClient = $githubClient;
    }

    private function getStoreConfigValue($key)
    {
        return $this->scopeConfig->getValue($key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    protected function log($msg, $vars = null)
    {
        if ($vars !== null) {
            $msg .= json_encode($vars);
        }
        $this->logger->info(
            $msg,
            [
                'module' => 'Cotya_VcsMapper',
                'class' => 'Cron'
            ]
        );
    }

    public function repositoryRefresh()
    {
        $this->log('start method');
        try {
            $apiToken = $this->getStoreConfigValue('cotya_vcs_mapper/api/github_key');
            $this->githubClient->authenticate($apiToken, \Github\Client::AUTH_HTTP_TOKEN);
            $repository = $this->repositoryManager->getRepositoryForRefreshFromQueue();
            //$repository = $this->repositoryManager->getRepositoryByUri('git://github.com/Adyen/magento.git');
            $repositoryVersions = $this->repositoryManager->getVersionsOfRepository($repository);
            $tagsIndexed = [];
            foreach ($repositoryVersions as $version) {
                $tagsIndexed[$version->getTag()] = $version;
            }
            $repositoryUri = $repository->getUri();
            if (parse_url($repositoryUri, PHP_URL_HOST)==='github.com') {
                $this->log('current github rate Limit: ', $this->githubClient->rateLimit()->getRateLimits()['rate']);
                $path = parse_url($repositoryUri, PHP_URL_PATH);
                $path = str_replace('.git', '', trim($path, '/'));
                list($orgName,$repoName) = explode('/', $path);
                //ld($path, $orgName, $repoName);
                //$info = $this->githubClient->repo()->show($orgName, $repoName);
                $this->log('getting tags for '.$orgName.'/'.$repoName);
                $tags = $this->githubClient->repo()->tags($orgName, $repoName);
                $this->log('got tags for '.$orgName.'/'.$repoName ."(".count($tags).")");
                foreach ($tags as $tag) {
                    $this->log('start process tag '. $tag['name']);
                    try {
                        if (isset($tagsIndexed[$tag['name']])) {
                            continue;
                        }
                        $version = $this->repositoryManager->buildVersionObject($tag['name']);
                        $version->setTag($tag['name']);
                        $jsonObject = [];
                        try {
                            $jsonObject['composer_json_txt'] = $this->githubClient->repo()
                                ->contents()->download($orgName, $repoName, '/composer.json', $tag['commit']['sha']);
                        } catch (\Github\Exception\RuntimeException $e) {
                            if ($e->getMessage() === "Not Found") {
                                $jsonObject['composer_json_txt'] = null;
                            } else {
                                throw $e;
                            }
                        }
                        $version->setMeta(json_encode($jsonObject));
                        $this->repositoryManager->addVersionToRepository($repository, $version);
                        $this->log('added version for tag ', $tag['name']);
                    } catch (\Exception $e) {
                        //ld($e);
                    }
                }
                //ld($this->githubClient->rateLimit()->getRateLimits()['rate']);
            }
        } catch (\Exception $e) {
            throw $e;
            //ld($e);
        } finally {

            //ldd();
        }
        $this->log('finish method');
    }
}
