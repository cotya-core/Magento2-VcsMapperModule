<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Model;

/**
 * Class Repository
 *
 * @method setUri(string $uri)
 * @method string getUri()
 * 
 * @package Cotya\VcsMapper
 */
class Repository extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init('Cotya\VcsMapper\Model\Resource\Repository');
    }
}
