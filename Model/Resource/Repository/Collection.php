<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Model\Resource\Repository;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Cotya\VcsMapper\Model\Repository', 'Cotya\VcsMapper\Model\Resource\Repository');
    }
}
