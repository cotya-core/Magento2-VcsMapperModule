<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Model\Resource\Version;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Cotya\VcsMapper\Model\Version', 'Cotya\VcsMapper\Model\Resource\Version');
    }
}
