<?php
/**
 *
 *
 *
 *
 */

namespace Cotya\VcsMapper\Model\Resource;


class Version extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('cotya_vcsmapper_version', 'version_id');
    }
    
}
